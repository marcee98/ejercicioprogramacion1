<!DOCTYPE html>
<html>
<head>
    <title>Calculadora</title>
</head>
<body>
    <h1>Calculadora</h1>
    <form method="post">
        <input type="number" name="operando1" placeholder="Operando 1" required><br>
        <input type="number" name="operando2" placeholder="Operando 2" required><br>
        <select name="operacion">
            <option value="suma">Suma</option>
            <option value="resta">Resta</option>
            <option value="multiplicacion">Multiplicación</option>
            <option value="division">División</option>
        </select><br>
        <input type="submit" name="calcular" value="Calcular">
    </form>

    <?php
    if(isset($_POST['calcular'])){
        $operando1 = $_POST['operando1'];
        $operando2 = $_POST['operando2'];
        $operacion = $_POST['operacion'];

        if($operacion == "suma"){
            $resultado = $operando1 + $operando2;
        }
        elseif($operacion == "resta"){
            $resultado = $operando1 - $operando2;
        }
        elseif($operacion == "multiplicacion"){
            $resultado = $operando1 * $operando2;
        }
        elseif($operacion == "division"){
            if($operando2 != 0){
                $resultado = $operando1 / $operando2;
            }
            else{
                echo "Error: No se puede dividir por cero.";
                exit;
            }
        }

        echo "Resultado: $resultado";
    }
    ?>
</body>
</html>
