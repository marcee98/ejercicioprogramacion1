<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Buscar en Agenda</title>
</head>
<body>
    <h1>Buscar en Agenda</h1>
    <form action="" method="post">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required><br><br>
        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido" required><br><br>
        <button type="submit">Buscar</button>
    </form>

    <?php
    function buscarEnAgenda($nombre, $apellido) {
        $archivo = 'agenda.txt';
        $encontrado = false;
        $contenido = file($archivo, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if ($contenido) {
            foreach ($contenido as $linea) {
                list($nombreArchivo, $apellidoArchivo) = explode(' ', $linea);

                if (trim($nombreArchivo) === $nombre && trim($apellidoArchivo) === $apellido) {
                    $encontrado = true;
                    break;
                }
            }
        }

        return $encontrado;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];

        if (buscarEnAgenda($nombre, $apellido)) {
            echo "<p>¡El nombre $nombre $apellido fue encontrado en la agenda!</p>";
        } else {
            echo "<p>El nombre $nombre $apellido no fue encontrado en la agenda.</p>";
        }
    }
    ?>
</body>
</html>
