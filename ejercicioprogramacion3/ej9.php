<?php
function sumarVector($vector) {
    $suma = 0;
    foreach ($vector as $elemento) {
        $suma += $elemento;
    }
    return $suma;
}

$vector = array(); 
for ($i = 0; $i < 100; $i++) {
    $vector[] = rand(1, 100);
}

$total = sumarVector($vector);
echo "Vector generado: [" . implode(", ", $vector) . "]<br>";
echo "La sumatoria de los elementos del vector es: $total";
?>
