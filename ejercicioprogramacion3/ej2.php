<?php
echo "Versión de PHP utilizada: " . phpversion() . "<br>";
echo "ID de la versión de PHP: " . phpversion('id') . "<br>";
echo "Valor máximo soportado para enteros: " . PHP_INT_MAX . "<br>";
echo "Tamaño máximo del nombre de un archivo: " . PHP_MAXPATHLEN . "<br>";

echo "Versión del Sistema Operativo: " . php_uname() . "<br>";

echo "Símbolo correcto de 'Fin De Línea': " . PHP_EOL . "<br>";

echo "Include path por defecto: " . get_include_path() . "<br>";
?>
