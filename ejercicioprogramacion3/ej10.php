<?php
function encontrarMaximo($vector) {
    $maximo = $vector[0];
    $indice = 0;
    for ($i = 1; $i < count($vector); $i++) {
        if ($vector[$i] > $maximo) {
            $maximo = $vector[$i];
            $indice = $i;
        }
    }
    return array("maximo" => $maximo, "indice" => $indice);
}

$vector = array(); 
for ($i = 0; $i < 50; $i++) {
    $vector[] = rand(1, 1000);
}

$resultado = encontrarMaximo($vector);
$maximo = $resultado["maximo"];
$indice = $resultado["indice"];

echo "El elemento con índice $indice posee el mayor valor y es: $maximo.<br>";

echo "Vector generado: [" . implode(", ", $vector) . "]<br>";
?>
