<!DOCTYPE html>
<html>
<head>
    <title>Verificar Cadena en Array</title>
</head>
<body>
    <h1>Verificar Cadena en Array</h1>

    <?php
    session_start();


    if (!isset($_SESSION['cadenas'])) {
        $_SESSION['cadenas'] = array("cadena1", "cadena2", "cadena3", "cadena4", "cadena5", "cadena6", "cadena7", "cadena8", "cadena9", "cadena10");
    }

 
    $nuevaCadena = "";


    if (isset($_POST['nuevaCadena'])) {
        $nuevaCadena = $_POST['nuevaCadena'];


        function buscarCadena($cadena, $array) {
            return in_array($cadena, $array);
        }


        if (buscarCadena($nuevaCadena, $_SESSION['cadenas'])) {
            echo "La cadena \"$nuevaCadena\" ya existe en el array.";
        } else {
            echo "La cadena \"$nuevaCadena\" es nueva y se ha agregado al final del array.";
            $_SESSION['cadenas'][] = $nuevaCadena;
        }
    }
    ?>

    <form method="post">
        <label for="nuevaCadena">Introduce una nueva cadena:</label>
        <input type="text" name="nuevaCadena" id="nuevaCadena" required>
        <input type="submit" value="Verificar">
    </form>


    <h2>Array actualizado:</h2>
    <pre>
        <?php
        print_r($_SESSION['cadenas']);
        ?>
    </pre>
</body>
</html>
