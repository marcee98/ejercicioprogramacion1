<!DOCTYPE html>
<html>
<head>
    <title>Registro de Acceso</title>
</head>
<body>
    <h1>Registro de Acceso</h1>

    <?php
    $logAcceso = array(
        "www.abc.com.py",
        "www.mail.google.com",
        "www.abc.com.py",
        "www.abc.com.py",
        "www.gitreaf.org",
        "www.mail.google.com",
        "www.gitreaf.org",
        "www.abc.com.py",
        "www.mail.google.com",
        "www.abc.com.py",
		"www.hattrick.com",
		"www.audiavirtual.uc.edu.py",
		"www.audiavirtual.uc.edu.py"
    );

    $accesosPorURL = array();

    foreach ($logAcceso as $url) {
        if (isset($accesosPorURL[$url])) {
            $accesosPorURL[$url]++;
        } else {
            $accesosPorURL[$url] = 1;
        }
    }

    echo "<h2>Enlaces disponibles:</h2>";
    echo "<ul>";
    foreach ($accesosPorURL as $url => $accesos) {
        echo "<li><a href='http://$url' target='_blank'>$url</a> (Accesos: $accesos)</li>";
    }
    echo "</ul>";
    ?>
</body>
</html>
