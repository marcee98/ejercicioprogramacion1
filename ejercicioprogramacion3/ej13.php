<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 13 - PHP en HTML</title>
</head>
<body>
    <h1>Ejercicio 13 - PHP en HTML</h1>

    <?php

    $array10 = array(
        "clave1" => "valor1",
        "clave2" => "valor2",
        "clave3" => "valor3",
        "clave4" => "valor4",
        "clave5" => "valor5",
        "clave6" => "valor6",
        "clave7" => "valor7",
        "clave8" => "valor8",
        "clave9" => "valor9",
        "clave10" => "valor10"
    );

    if (isset($_POST['nuevaClave']) && isset($_POST['nuevoValor'])) {
        $nuevaClave = $_POST['nuevaClave'];
        $nuevoValor = $_POST['nuevoValor'];

        $claveExistente = array_key_exists($nuevaClave, $array10);

        $valorExistente = in_array($nuevoValor, $array10);

        if ($claveExistente && $valorExistente) {
            echo "La clave '$nuevaClave' y el valor '$nuevoValor' ya existen en el array de 10 elementos.<br>";
        } elseif ($claveExistente) {
            echo "La clave '$nuevaClave' ya existe en el array de 10 elementos, pero el valor '$nuevoValor' es nuevo.<br>";
        } elseif ($valorExistente) {
            echo "El valor '$nuevoValor' ya existe en el array de 10 elementos, pero la clave '$nuevaClave' es nueva.<br>";
        } else {
            $array10[$nuevaClave] = $nuevoValor;
            echo "Se ha insertado 'Clave: $nuevaClave' y 'Valor: $nuevoValor' en el array de 10 elementos.<br>";
        }
    }
    ?>

    <h2>Ingrese una nueva clave y valor:</h2>
    <form method="post">
        <label for="nuevaClave">Nueva Clave:</label>
        <input type="text" name="nuevaClave" id="nuevaClave" required><br>
        <label for="nuevoValor">Nuevo Valor:</label>
        <input type="text" name="nuevoValor" id="nuevoValor" required><br>
        <input type="submit" value="Verificar">
    </form>

    <h2>Array actualizado:</h2>
    <pre>
        <?php
        print_r($array10);
        ?>
    </pre>
</body>
</html>
