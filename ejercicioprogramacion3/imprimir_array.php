<?php
function imprimirArray($array) {
    echo "Array completo:<br>";
    foreach ($array as $clave => $valor) {
        echo "Clave: $clave, Valor: $valor<br>";
    }

    $letrasBuscadas = array('a', 'd', 'm', 'z');
    $clavesEncontradas = array();

    foreach ($array as $clave => $valor) {
        $primeraLetra = substr($clave, 0, 1);
        if (in_array($primeraLetra, $letrasBuscadas)) {
            $clavesEncontradas[] = $clave;
        }
    }

    if (count($clavesEncontradas) > 0) {
        echo "Claves que empiezan con las letras a, d, m o z:<br>";
        foreach ($clavesEncontradas as $clave) {
            echo "$clave<br>";
        }
    } else {
        echo "No se encontraron claves que empiecen con las letras a, d, m o z.<br>";
    }
}
?>
