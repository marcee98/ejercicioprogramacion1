<?php
// Función para generar números aleatorios
function generarNumeroAleatorio() {
    return rand(1, 100000); // Se genera un número aleatorio entre 1 y 100000
}

// Ciclo infinito
while (true) {
    $numero = generarNumeroAleatorio(); // Generar un número aleatorio
    
    if ($numero % 983 === 0) {
        echo "¡El número $numero es divisible por 983!";
        break; // Salir del ciclo cuando se cumple la condición
    }
}

?>
