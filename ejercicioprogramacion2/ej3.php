<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manipulación de Variables en PHP</title>
</head>

<body>

    <?php
// Crear una variable con el contenido 24.5
$variable = 24.5;

// Imprimir el tipo de datos de la variable
echo "Tipo de datos original: " . gettype($variable) . "<br>";

// Modificar el contenido de la variable
$variable = "HOLA";

// Imprimir el tipo de datos de la variable después de la modificación
echo "Tipo de datos después de la modificación: " . gettype($variable) . "<br>";

// Setear el tipo de la variable a un tipo entero
settype($variable, "integer");

// Imprimir el contenido y tipo de la variable con var_dump
echo "Contenido y tipo después de setear a entero: ";
var_dump($variable);
?>

</body>

</html>