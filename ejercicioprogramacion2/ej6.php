<?php
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);


$total = $parcial1 + $parcial2 + $final1;


switch (true) {
    case $total >= 90 && $total <= 100:
        $nota = "Nota 5";
        break;
    case $total >= 80 && $total <= 89:
        $nota = "Nota 4";
        break;
    case $total >= 70 && $total <= 79:
        $nota = "Nota 3";
        break;
    case $total >= 60 && $total <= 69:
        $nota = "Nota 2";
        break;
    case $total >= 0 && $total <= 59:
        $nota = "Nota 1";
        break;
    default:
        $nota = "Nota inválida";
        break;
}


echo "Parcial1: $parcial1";
echo "Parcial2: $parcial2";
echo "Final1: $final1";
echo "El alumno obtuvo: $nota";
?>
