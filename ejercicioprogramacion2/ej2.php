<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Concatenar Cadenas con PHP</title>
</head>

<body>

    <?php
// Definir dos cadenas en variables
$cadena1 = "Hola";
$cadena2 = "mundo!";

// Concatenar las dos cadenas
$resultado = $cadena1 . $cadena2;

// Imprimir el resultado
echo "<p>$resultado</p>";
?>

</body>

</html>