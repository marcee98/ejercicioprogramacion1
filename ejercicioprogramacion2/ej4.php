<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Multiplicar del 9</title>
    <style>
    /* Estilo para filas alternadas */
    tr:nth-child(even) {
        background-color: #f2f2f2;
    }
    </style>
</head>

<body>

    <table border="1">
        <tr>
            <th>Multiplicando</th>
            <th>Resultado</th>
        </tr>

        <?php
    // Calcular la tabla de multiplicar del 9
    for ($i = 1; $i <= 10; $i++) {
        // Determinar el color de fondo de la fila
        $color = ($i % 2 == 0) ? 'white' : 'grey';

        // Calcular el resultado de la multiplicación
        $resultado = 9 * $i;

        // Imprimir la fila de la tabla
        echo "<tr style='background-color: $color;'>
                <td>9 x $i</td>
                <td>$resultado</td>
              </tr>";
    }
    ?>
    </table>

</body>

</html>