import React from "react";

export default function Ayudanos() {
  return (
    <div className="h-screen bg-[#143f6e] flex items-center justify-center pt-[60px]">
      <div className="container">
        <h1>Ayudanos</h1>
        <div className="grid grid-cols-3">
          <div className="flex flex-row items-center justify-center text-white text-center">
            <div>1</div>
            <div>2</div>
            <div>3</div>
          </div>
        </div>
      </div>
    </div>
  );
}
