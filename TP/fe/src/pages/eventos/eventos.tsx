import React from "react";
import EventCard from "../../components/eventCard";

export default function Eventos() {
  const eventos = [
    {
      imagen: "/src/assets/evento-hamburgueseada.jpeg",
      titulo: "Hamburgueseada",
      lugar: "Rio Parana 360 entre felicidad y cacique sepe",
      descripcion: "",
    },
    {
      imagen: "/src/assets/evento-postres.jpeg",
      titulo: "Postres",
      lugar: "Plaza Techada",
      descripcion: "",
    },
    {
      imagen: "/src/assets/evento-colecta.jpeg",
      titulo: "Colecta",
      lugar: "Puntos de Colecta",
      descripcion: "",
    },
  ];

  return (
    <div className="bg-[#143f6e] flex justify-center py-[30px]">
      <div className="container text-white mx-[100px]">
        <h1 className="text-7xl font-bold text-center pb-[30px]">Eventos</h1>
        <div className="grid grid-cols-3 auto-rows-auto gap-5">
          {eventos.map((evento: any, index: number) => {
            return (
              <EventCard
                key={index}
                imagen={evento.imagen}
                titulo={evento.titulo}
                lugar={evento.lugar}
                descripcion={evento.descripcion}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
