import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/team-padrinos-uca-logo.jpeg";

function Conocenos() {
  return (
    <div id="conocenos" className="bg-[#ADD8E6] py-20 text-[#0e3d69]">
      <h1 className="text-center text-6xl font-bold pb-10">Conocenos</h1>
      <div className="grid grid-cols-2 mx-[200px]">
        <div className="flex flex-col items-center justify-center text-center">
          <div>
            <h2 className="text-5xl font-bold">Quienes somos?</h2>
            <p className="pt-5 text-lg">
              Nos enorgullece contar con un equipo diverso y multidisciplinario,
              compuesto por apasionados alumnos provenientes de diversas
              carreras y facultades de la Universidad Católica. Esta amalgama de
              conocimientos y perspectivas enriquece nuestra labor y nos permite
              abordar los desafíos desde diferentes ángulos, promoviendo un
              ambiente colaborativo y creativo. Contribuyendo a la construcción
              de un refugio de segundas oportunidades y marcando la diferencia
              en la vida de aquellos a quienes servimos.
            </p>
          </div>
        </div>
        <div className="flex items-center justify-center">
          <img className="w-[500px] h-[500px] rounded-full" src={logo} />
        </div>
      </div>
      <div className="grid grid-cols-2 mx-[200px]">
        <div className="flex items-center justify-center">
          <img className="w-[500px] h-[500px] rounded-full" src={logo} />
        </div>
        <div className="flex flex-col items-center justify-center text-center">
          <div>
            <h3 className="text-5xl font-bold">Por que hacemos lo que hacemos?</h3>
            <p className="pt-5 text-lg">
            Nos embarcamos en esta noble misión porque, al encontrarnos con esos adorables compañeros peludos tanto dentro como fuera del campus. La presencia constante de estos seres leales y su necesidad de apoyo despertaron en nosotros un profundo deseo de marcar la diferencia en sus vidas.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default function HomePage() {
  // const [ showConocenos, setShotConocenos ] = useState(false);

  // useEffect(() => {
  //   const handleScroll = () => {
  //     const scrollPosition = window.scrollY;

  //     const scrollThreshhold = 0;

  //     setShotConocenos(scrollPosition > scrollThreshhold);
  //   };
  //   window.addEventListener("scroll", handleScroll);

  //   return () => {
  //     window.removeEventListener("scroll", handleScroll);
  //   };
  // }, [])

  return (
    <>
      <div className="bg-[#143f6e] flex items-center justify-center">
        <div className="container">
          <div className="grid grid-cols-2 mx-[200px] py-[70px]">
            <div className="flex flex-col items-center justify-center text-white text-center">
              <div>
                <h1 className="text-7xl font-bold">Team Padrinos UCA</h1>
                <p className="pt-5">
                  Construyendo un refugio de segundas oportunidades. Cada
                  rescate es una historia de esperanza, cada adopción es un
                  nuevo comienzo. Únete a nosotros, cambia vidas juntos.
                </p>
                <div className="flex flex-row justify-center gap-16 pt-10">
                  <Link to="/animales">
                    <button className="bg-[#2ea8db] w-[180px] text-lg px-10 py-5 rounded-full hover:bg-[#1c7bbd] transition duration-300 ease-in-out hover:border-transparent border-2 border-[#2ea8db]">
                      Adopta
                    </button>
                  </Link>
                  <Link to="/ayudanos">
                    <button className="bg-transparent border-2 border-[#2ea8db] w-[180px] text-lg px-10 py-5 rounded-full hover:bg-[#1c7bbd] hover:border-[#1c7bbd] transition duration-300 ease-in-out">
                      Apadrina
                    </button>
                  </Link>
                </div>
              </div>
            </div>
            <div className="flex items-center justify-center">
              <img className="w-[500px] h-[500px] rounded-full" src={logo} />
            </div>
          </div>
          {/* {showConocenos && <Conocenos />} */}
        </div>
      </div>
      <Conocenos />
    </>
  );
}
