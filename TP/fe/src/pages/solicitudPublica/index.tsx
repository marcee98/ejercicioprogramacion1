import React, { useState } from "react";
import {
    Card,
    Input,
    Checkbox,
    Button,
    Typography,
    Collapse,
    CardBody,
} from "@material-tailwind/react";
import DynamicForm from "../../components/dynamicForm/dynamicForm";

function CollapseDefault() {
    const [open, setOpen] = React.useState(false);
    const [formData, setFormData] = useState({});

    const toggleOpen = () => setOpen((cur) => !cur);

    // Define tu objeto con la información de los campos
    const formFields = [
        { name: 'ci', label: 'Cedula de Identidad', type: 'text' },
        { name: 'nombre', label: 'Nombre', type: 'text' },
        { name: 'apellido', label: 'Apellido', type: 'text' },
        { name: 'correo_electronico', label: 'Correo Electrónico', type: 'text' },
        { name: 'telefono', label: 'Teléfono', type: 'text' },
        { name: 'direccion', label: 'Dirección', type: 'text' },
        { name: 'ocupacion', label: 'Ocupación', type: 'text' },
    ];

    const handleFieldChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({ ...prevData, [name]: value }));
    };

    const handleButtonClick = () => {
        // Aquí puedes manejar la lógica cuando se hace clic en el botón
        console.log('FormData:', formData);
    };

    return (
        <>
            {/* <Button onClick={toggleOpen}>Datos del adoptante</Button>
            <Collapse open={open} className="">
                <Card className="my-4 mx-auto w-12/12">
                    <CardBody>
                        
                            <div className="border-b border-gray-900/10 pb-12">
                                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                    <div className="sm:col-span-2 sm:col-start-1">
                                        <label htmlFor="city" className="block text-sm font-medium leading-6 text-gray-900">City</label>
                                        <div className="mt-2">
                                            <input type="text" name="city" id="city" autoComplete="address-level2" className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                                        </div>
                                    </div>

                                    <div className="sm:col-span-2">
                                        <label htmlFor="region" className="block text-sm font-medium leading-6 text-gray-900">State / Province</label>
                                        <div className="mt-2">
                                            <input type="text" name="region" id="region" autoComplete="address-level1" className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                                        </div>
                                    </div>

                                    <div className="sm:col-span-2">
                                        <label htmlFor="postal-code" className="block text-sm font-medium leading-6 text-gray-900">ZIP / Postal code</label>
                                        <div className="mt-2">
                                            <input type="text" name="postal-code" id="postal-code" autoComplete="postal-code" className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                    </CardBody>
                </Card>
            </Collapse> */}
            <>
                <Button onClick={toggleOpen}>Datos del adoptante</Button>
                <Collapse open={open} className="">
                    <Card className="my-4 mx-auto w-12/12">
                        <CardBody>
                            <div className="border-b border-gray-900/10 pb-12">
                                <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
                                    {formFields.map((field) => (
                                        <div key={field.name} className="sm:col-span-2">
                                            <Typography variant="h6" color="blue-gray" className="mb-3">
                                                {field.label}
                                            </Typography>
                                            <div className="mt-2">
                                                <Input
                                                    type={field.type}
                                                    name={field.name}
                                                    id={field.name}
                                                    autoComplete={field.autoComplete}
                                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                                                    onChange={handleFieldChange} crossOrigin={undefined}                                                />
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                    <Button onClick={handleButtonClick}>Obtener Datos</Button>
                </Collapse>
            </>
        </>
    );
}


export default function SolicitudPublica() {
    const formFields = [
        { name: 'ci', label: 'Cedula de Identidad', type: 'text' },
        { name: 'nombre', label: 'Nombre', type: 'text' },
        { name: 'apellido', label: 'Apellido', type: 'text' },
        { name: 'correo_electronico', label: 'Correo Electrónico', type: 'text' },
        { name: 'telefono', label: 'Teléfono', type: 'text' },
        { name: 'direccion', label: 'Dirección', type: 'text' },
        { name: 'ocupacion', label: 'Ocupación', type: 'text' },
        {
            name: 'id_del_adoptante',
            label: 'Numero de cedula',
            type: 'text',
        },
        {
            name: 'id_del_animal',
            label: 'Animal',
            type: 'select',
            options: [
                { id: 1, text: 'Material Tailwind HTML' },
                { id: 2, text: 'Material Tailwind React' },
                { id: 3, text: 'Material Tailwind Vue' },
                { id: 4, text: 'Material Tailwind Angular' },
                { id: 5, text: 'Material Tailwind Svelte' },
            ],
        },
        {
            name: 'datos_adicionales',
            label: 'Additional Data',
            type: 'text',
        },
    ];


    // Función para manejar la presentación de la solicitud
    const handleSolicitudSubmit = (formData) => {
        // Puedes enviar la solicitud al servidor aquí o realizar acciones adicionales
        console.log('Solicitud enviada:', formData);
    };
    return (
        <div className="bg-all py-20 text-[#0e3d69]">
            <h1 className="text-center text-6xl font-bold pb-10">Adopta</h1>

            <div className="grid  mx-[200px]">
                {/* <div className="mx-auto max-w-2xl p-6 bg-white rounded-md shadow-md"> */}

                {/* </div> */}
            </div>
            <div className="grid pt-10">

                <div className="flex flex-col items-center justify-center text-center">
                    {/* <div> */}
                    <h2 className="text-3xl font-bold mb-6">Formulario de Solicitud de Adopción</h2>
                    {/* <div className="text-center"><CollapseDefault /></div> */}
                    {/* <div className="grid gap-4 grid-cols-2">
                        <div>01</div>
                        <div>02</div>
                        <div>03</div>
                        <div>04</div>
                    </div> */}
                    <DynamicForm fields={formFields} onSubmit={handleSolicitudSubmit} titulo="" />
                    {/* <SimpleRegistrationForm/> */}
                    {/* </div> */}
                </div>
            </div>
        </div>

    );
}
