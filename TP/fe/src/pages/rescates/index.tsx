import React, { useEffect, useState } from "react";
// import { Card, IconButton, Typography } from "@material-tailwind/react";
import withMT from "@material-tailwind/react/utils/withMT";
import animalService from "../../services/animalService";
import rescateService from "../../services/rescateService";
import loginService from "../../services/loginService";

import {
    IconButton,
    Button,
    Dialog,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Typography,
    Input,
    Checkbox,
    Select,
    Option,
} from "@material-tailwind/react";
import { useAuth } from "../../context/authProvider";
import { all } from "axios";

const DynamicSelect = ({ label, onChange, options }) => {
    return (
        <div key={label}>
            <Select label={label} onChange={onChange} size="md">
                {options.map((option) => (
                    <Option key={option.id} value={String(option.id)}>
                        {option.text}
                    </Option>
                ))}
            </Select>
        </div>
    );
};

export function DynamicFormDialogg({
    fields,
    onSubmit,
    titulo,
    iconButton = false,
    initialValues = {},
    row = {
        "id": null,
        "id_usuario": 1,
        "estado": "En curso",
        "direccion": "Dirección del rescate",
        "fecha_rescate": "2023-11-19 00:00:00",
        "informacion_adicional": "string",
        "id_animal": 1,
        "created_at": "2023-11-27T01:34:38.000000Z",
        "updated_at": "2023-11-27T01:34:38.000000Z",
        "animal": {
            "id": 1,
            "nombre": "Nombre del animal",
            "especie": "Especie del animal",
            "tamano": 2,
            "edad": 3,
            "descripcion": "Descripción del animal",
            "created_at": "2023-11-27T01:34:38.000000Z",
            "updated_at": "2023-11-27T01:34:38.000000Z"
        }
    }
}) {
    const [open, setOpen] = React.useState(false);
    const [formData, setFormData] = React.useState(initialValues);
    const [valueForm, setValueForm] = useState({
        Nombre: "Valor inicial para Nombre",
        Especie: "Valor inicial para Especie",
        Tamaño: 2, // Aquí puedes usar el ID de la opción predeterminada
        Edad: 3,
        Fecha_Rescate: "2023-01-01", // Valor inicial para Fecha de Rescat
        Informacion_Adicional: ""
    });

    const handleOpen = () => setOpen((cur) => !cur);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = () => {
        if (onSubmit) {
            // Llama a la función onSubmit con los datos del formulario
            console.log(formData)
            onSubmit(formData);
        }
        // Cierra el diálogo después de enviar el formulario
        handleOpen();
    };

    // Este efecto se ejecutará solo cuando initialValues cambie
    useEffect(() => {
        if (row.id != null) {
            const fechaOriginal = row.fecha_rescate;
            const fechaObjeto = new Date(fechaOriginal);
            const año = fechaObjeto.getFullYear();
            const mes = (fechaObjeto.getMonth() + 1).toString().padStart(2, '0');
            const dia = fechaObjeto.getDate().toString().padStart(2, '0');
            const fechaFormateada = `${año}-${mes}-${dia}`;
            setValueForm({
                Nombre: row.animal.nombre,
                Especie: row.animal.especie,
                Tamaño: 1, // Aquí puedes usar el ID de la opción predeterminada
                Edad: row.animal.edad,
                Fecha_Rescate: fechaFormateada,
                Informacion_Adicional: row.informacion_adicional // Valor inicial para Fecha de Rescat
            })

            setFormData(valueForm)
            console.log("prueba", initialValues)

        }
    }, [open]);

    return (
        <>
            <div className="w-40">
                {iconButton ? (
                    <IconButton ripple={true} onClick={handleOpen}>
                        <i className="fas fa-edit" />
                    </IconButton>
                ) : (
                    <Button onClick={handleOpen}>{titulo}</Button>
                )}
            </div>
            <Dialog
                size="xs"
                open={open}
                handler={handleOpen}
                className="bg-transparent shadow-none">
                <Card className="mx-auto w-full max-w-[24rem]">
                    <CardBody className="flex flex-col gap-4">
                        {fields.map((field) => (
                            <React.Fragment key={field.label}>
                                <Typography className="-mb-2" variant="h6">
                                    {field.label}
                                </Typography>
                                {field.type === "select" ? (
                                    <DynamicSelect
                                        label={field.label}
                                        options={field.options || []}
                                        onChange={(selectedValue) =>
                                            handleChange({
                                                target: { name: field.label, value: selectedValue },
                                            })
                                        }
                                    />
                                ) : (
                                    <Input
                                        label={field.label}
                                        size="lg"
                                        type={field.type || "text"}
                                        name={field.label}
                                        value={formData[field.label] || ""}
                                        onChange={handleChange}
                                    />
                                )}
                            </React.Fragment>
                        ))}
                    </CardBody>
                    <CardFooter className="pt-0">
                        <Button variant="gradient" onClick={handleSubmit} fullWidth>
                            Submit
                        </Button>
                    </CardFooter>
                </Card>
            </Dialog>
        </>
    );
}




const DynamicTable = ({ data, formFields, onEdit, selectedColumns = [], handleDelete }) => {
    // Obtener todas las columnas de todos los objetos
    const columns = Array.from(new Set(data.flatMap((item) => Object.keys(item))));

    // Filtrar las columnas si se proporcionan columnas seleccionadas
    const visibleColumns = selectedColumns.length > 0 ? selectedColumns : columns;


    return (
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        {visibleColumns.map((column, index) => (
                            <th key={index} className="px-6 py-3">
                                {String(column)}
                            </th>
                        ))}
                        <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Edit</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((row, rowIndex) => (
                        <tr
                            key={rowIndex}
                            className={`${rowIndex % 2 === 0 ? "bg-white" : "bg-white dark:bg-gray-800"
                                } border-b dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600`}>
                            {selectedColumns.map((column, colIndex) => (
                                <td key={colIndex} className="px-6 py-4 whitespace-nowrap">
                                    <Typography variant="small" color="blue-gray" className="font-normal">
                                        {row.animal[column] || row[column]}
                                    </Typography>
                                </td>
                            ))}
                            <td className="px-6 py-4 text-right">
                                <div className="grid grid-cols-2">
                                    <div className="w-min">
                                        <DynamicFormDialogg
                                            fields={formFields}
                                            onSubmit={() => onEdit(row)}
                                            titulo={"Editar"}
                                            iconButton={true}
                                            row={row}
                                        />

                                    </div>
                                    <div className="w-min">
                                        <IconButton className="bg-red-500" ripple={true} onClick={() => { handleDelete(row.id) }}>
                                            <i className="fas fa-trash" />
                                        </IconButton>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default function Rescates() {
    const context = useAuth();
    console.log(context);
    const [exampleData, setExampleData] = useState([]);
    const [updateData, setUpdateData] = useState(false);

    const handleDelete = async (id) => {
        if (id != null) {
            try {
                await rescateService.deleteRescate(id);
                setUpdateData((prevUpdateData) => !prevUpdateData);
                setExampleData((prevData) => prevData.filter((item) => item.id !== id));
            } catch (error) {
                console.error("Error deleting data:", error);
            }
        }
    };



    useEffect(() => {
        console.log("DEENI")
        const fetchAndMapData = async () => {
            try {
                const info = await rescateService.GetAllRescates();
                console.log("Datos", info);

                // Perform mapping here
                // const mappedData = info.map((rescate) => {
                //     const { animal, fecha_rescate } = rescate;

                //     return {
                //         nombre: animal.nombre,
                //         especie: animal.especie,
                //         tamaño: animal.tamano,
                //         edad: animal.edad,
                //         fecha: fecha_rescate,
                //     };
                // });

                // console.log("Mapped Data", mappedData);
                setExampleData(info); // Set the state with the mapped data
            } catch (error) {
                console.error("Error fetching data:", error);
                // Handle the error as needed
                setExampleData([]); // Return an empty array or handle the error accordingly
            }
        };

        fetchAndMapData(); // Call the function when the component mounts
    }, [updateData]);

    // const exampleData = [
    //   {
    //     nombre: "Frodito",
    //     especie: "Perro",
    //     tamaño: "Mediano",
    //     edad: "5",
    //     fecha: "10/12/2023",
    //   },
    //   {
    //     nombre: "Frodito",
    //     especie: "Perro",
    //     tamaño: "Mediano",
    //     edad: "5",
    //     fecha: "10/12/2023",
    //   },
    //   {
    //     nombre: "Frodito",
    //     especie: "Perro",
    //     tamaño: "Mediano",
    //     edad: "5",
    //     fecha: "10/12/2023",
    //   },
    // ];

    const handleEdit = (rowData) => {
        // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
        console.log("VVVV")
        console.log("Editando:", rowData);
    };

    const exampleColumns = ["Nombre", "Especie", "Tamaño", "Edad", "Fecha"];

    const formFields = [
        // { label: "Estado", type: "text" },
        { label: "Nombre", type: "text" },
        { label: "Especie", type: "text" },
        {
            label: "Tamaño",
            type: "select",
            options: [
                {
                    id: 1,
                    text: "Pequeño",
                },
                {
                    id: 2,
                    text: "Mediano",
                },
                {
                    id: 3,
                    text: "Grande",
                },
            ],
        },
        { label: "Edad", type: "text" },
        { label: "Fecha_Rescate", type: "date" },
        { label: "Informacion_Adicional", type: "text" },
        // {
        //     label: "Animal", type: "select", options: [
        //         { id: 1, text: 'Opción 1' },
        //         { id: 2, text: 'Opción 2' },
        //         { id: 3, text: 'Opción 3' },
        //     ]
        // },
    ];

    //crear rescate
    const handleSubmit = async (data) => {
        try {
            const user = await loginService.GetUser(context.token);
            console.log(user.user);
            const formattedData = {
                id_usuario: user.user.id,
                direccion: data.Direccion || "Dirección del rescate",
                estado: data.Estado || "En curso",
                fecha_rescate: data.Fecha_Rescate || "2023-11-19",
                informacion_adicional: data.Informacion_Adicional || "string",
                id_animal: {
                    nombre: data.Nombre || "Nombre del animal",
                    especie: data.Especie || "Especie del animal",
                    tamano: data.Tamaño || "tamano del animal",
                    edad: data.Edad || 3,
                    descripcion: data.Descripcion || "Descripción del animal",
                },
            };
    
            console.log(data);
            
            // Hacer la llamada a la función asincrónica
            await rescateService.CreateRescate(formattedData);
    
            // Actualizar el estado para activar el useEffect
            setUpdateData((prevUpdateData) => !prevUpdateData);
        } catch (error) {
            console.error("Error fetching user data:", error);
            // Handle the error as needed
        }
    };

    const selectedColumns = [
        "nombre",
        "especie",
        "tamano",
        "edad",
        "fecha_rescate",
    ];
    return (
        <div className="bg-all py-20 text-[#0e3d69]">
            <h1 className="text-center text-6xl font-bold pb-10">Rescates</h1>
            <div className="grid gap-y-4  mx-[200px]">
                {/* <TabComponent /> */}
                <DynamicFormDialogg
                    fields={formFields}
                    onSubmit={handleSubmit}
                    titulo={"Nuevo Rescate"}
                />

                <DynamicTable
                    formFields={formFields}
                    data={exampleData}
                    selectedColumns={selectedColumns}
                    // columns={exampleColumns}
                    handleDelete={handleDelete}
                    onEdit={handleEdit}
                />
            </div>
            <div className="grid pt-10 mx-[200px]">
                <div className="flex flex-col items-center justify-center text-center">
                    <div>
                        {/* <h3 className="text-5xl font-bold">Por que hacemos lo que hacemos?</h3>
                        <p className="pt-5 text-lg">
                            Nos embarcamos en esta noble misión porque, al encontrarnos con esos adorables compañeros peludos tanto dentro como fuera del campus. La presencia constante de estos seres leales y su necesidad de apoyo despertaron en nosotros un profundo deseo de marcar la diferencia en sus vidas.
                        </p> */}
                    </div>
                </div>
            </div>
        </div>
    );
}