import React from "react";
import AnimalCard from "../../components/animalCard";

export default function Animales() {

  const animales = [
    {
      imagen: "/src/assets/perro-ex-1.jpg",
      nombre: "Frodito",
      descripcion:
        "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
      slug: "frodito",
    },
    {
      imagen: "/src/assets/perro-ex-2.jpg",
      nombre: "Pompita",
      descripcion:
        "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
      slug: "pompita",
    },
    {
      imagen: "/src/assets/perro-ex-3.jpg",
      nombre: "Negri",
      descripcion:
        "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
      slug: "negri",
    },
    ,
    {
      imagen: "/src/assets/perro-ex-3.jpg",
      nombre: "Blanqui",
      descripcion:
        "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
      slug: "blanqui",
    },
  ];

  return (
    <div className="bg-[#143f6e] flex justify-center py-[30px]">
      <div className="container text-white mx-[100px]">
        <h1 className="text-7xl font-bold text-center pb-[30px]">Animales</h1>
        <div className="grid grid-cols-3 auto-rows-auto gap-5">
          {animales.map((animal: any, index: number) => {
            return (
              <AnimalCard
                key={index}
                imagen={animal.imagen}
                nombre={animal.nombre}
                descripcion={animal.descripcion}
                slug={animal.slug}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
