import axios from "axios";
// import { useAuth } from "../context/authProvider";

const loginUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/login';
const logoutUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/logout';
const url = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/getaccount";
// const { token } = useAuth();

const Login = async credentials => {
    const { data } = await axios.post(loginUrl, credentials)
    return data;
}

const Logout = async (token) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}` 
        }
    }
    const request = axios.post(logoutUrl, null , config);
    return request.then(response => response.data);
}

const GetUser = async (token) => {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
  
    try {
      const response = await axios.get(url, config);
      console.log(response.data); // Assuming the data is what you expect
      return response.data;
    } catch (error) {
      console.error("Error fetching user data:", error);
      throw error; // You might want to handle or log this error appropriately
    }
  };

export default { Login, Logout, GetUser }
