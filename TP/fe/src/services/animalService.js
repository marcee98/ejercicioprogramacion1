import axios from "axios";

const animalUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/animales';

const GetAllAnimals = () => {
    const request = axios.get(animalUrl);
    return request.then(response => response.data)
}

const CreateAnimal = (newAnimal, {token}) => {
    const config = {
        headers: {
            Authorization: `Bearer ${token}` 
        }
    }
    const request = axios.post(animalUrl, newAnimal, config);
    return request.then(response => response.data);
}


export default { GetAllAnimals, CreateAnimal }