import React, { useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { Link as ScrollLink, animateScroll as scroll } from "react-scroll";
import logo from "../../../assets/team-padrinos-uca-logo.jpeg";
import Login from "../../login";

export default function NavBar() {
  const navigate = useNavigate();

  const scrollToConocenos = () => {
    // Si estamos en otra página que no sea la principal, navega a la principal y realiza el desplazamiento
    if (window.location.pathname !== "/") {
      navigate("/", { state: { scrollToConocenos: true } });
    } else {
      // Si ya estamos en la página principal, realiza solo el desplazamiento
      scroll.scrollTo(document.getElementById('conocenos').offsetTop, {
        duration: 500,
        smooth: 'easeInOutQuad',
      });
    }
  };

  const [login, setLogin ] = useState(false);
  const toggleLogin = () => {
    setLogin(!login);
    
  };

  return (
    <>
      <nav className="flex flex-wrap items-center justify-between px-2 py-3 bg-[#223b58]">
        <div className="container px-4 mx-auto flex items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white"
              to="/">
              <img className="w-[65px] h-auto rounded-full" src={logo} />
            </Link>
          </div>
          <div>
            <ul className="flex flex-row gap-10">
              <li className="nav-item">
                <ScrollLink
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out hover:cursor-pointer"
                  to="conocenos"
                  smooth={true}
                  duration={500}
                  onClick={scrollToConocenos}>
                  <span className="">Conocenos</span>
                </ScrollLink>
              </li>
              <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/animales">
                  <span className="">Animales</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/ayudanos">
                  <span className="">Ayudanos</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/eventos">
                  <span className="">Eventos</span>
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/solicitudes">
                  <span className="">Solicitud de adopcion</span>
                </Link>
              </li>
              <li className="nav-item">
                <Login/>
                {/* <button onClick={toggleLogin}
                  className="px-5 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out bg-[#2ea8db] rounded-2xl">
                  <span className="">Login</span>
                </button>
                {login && <Login toggleLogin={toggleLogin} />} */}
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div>
        <Outlet />
      </div>
    </>
  );
}
