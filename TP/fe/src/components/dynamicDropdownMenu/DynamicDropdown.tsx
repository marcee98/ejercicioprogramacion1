import React, { useState } from "react";
import { Link } from "react-router-dom";

const DynamicDropdown = ({ buttonText, links }) => {
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
  
    const toggleDropdown = () => {
      setIsDropdownOpen(!isDropdownOpen);
    };
  
    return (
      <div className="relative inline-block text-left">
        <button
          id="dropdownNavbarLink"
          data-dropdown-toggle="dropdownNavbar"
          className="flex mt-2 items-center justify-between w-full px-3 py-2 md:hover:opacity-75 text-sm uppercase transition duration-300 ease-in-out font-bold leading-snug md:dark:hover:text-gray-100 rounded md:border-0 md:p-0 md:w-auto dark:text-white dark:focus:text-white dark:border-gray-700 transition duration-300 ease-in-out"
          onClick={toggleDropdown}
        >
          {buttonText}
          <svg
            className="w-2.5 h-2.5 ms-2.5"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 10 6"
          >
            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 4 4 4-4" />
          </svg>
        </button>
  
        {/* Dropdown menu */}
        <div
          id="dropdownNavbar"
          className={`z-10 absolute ${isDropdownOpen ? 'block' : 'hidden'} font-normal bg-white divide-y divide-gray-100 rounded-lg shadow mt-2 w-44 colorr dark:divide-gray-600`}
        >
          <ul className="py-2 text-sm text-gray-700 dark:text-gray-400" aria-labelledby="dropdownLargeButton">
            {links.map((link, index) => (
              <li key={index}>
                <Link
                  to={link.to}
                  className="px-3 py-2 flex items-center text-sm font-bold leading-snug licolor text-white hover:opacity-75 transition duration-300 ease-in-out"
                >
                  {link.label}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  };

  export default DynamicDropdown;