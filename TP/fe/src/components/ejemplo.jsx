
// ExampleUsage.jsx
import React from 'react';
import DynamicForm from './dynamicForm/dynamicForm';


const ExampleUsage = () => {
  const handleSubmit = (data) => {
    // Handle form submission logic here
    console.log('Form Data:', data);
  };

  const formFields = [
    { name: 'firstName', label: 'First Name' },
    { name: 'lastName', label: 'Last Name' },
    { name: 'email', label: 'Email', type: 'email' },
  ];

  return (
    <div>
      <h1>Dynamic Deni</h1>
      <DynamicForm fields={formFields} onSubmit={handleSubmit} />
    </div>
  );
};

export default ExampleUsage;
