import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../context/authProvider";
import loginService from "../../services/loginService";
// import React, { useState } from "react";
import {
  Button,
  Dialog,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Checkbox,
} from "@material-tailwind/react";

// export default function Login({ toggleLogin }) {
//   const [nombre_usuario, setNombre_usuario] = useState("");
//   const [contrasena, setContrasena] = useState("");
//   const [usuario, setUsuario] = useState(null);
//   const [errorMessage, setErrorMessage] = useState("");
//   const { token, setToken, setCurrentUser, currentUser, isLoading, setIsLoading } = useAuth();
//   const navigate = useNavigate();

//   const handleUsuarioChange = (event) => {
//     setNombre_usuario(event.target.value);
//   };

//   const handlePasswordChange = (event) => {
//     setContrasena(event.target.value);
//   };

//   const handleLogin = async (event) => {
//     event.preventDefault();
//     setIsLoading(true);
//     try {
      
//       const credenciales = await loginService.Login({
//         nombre_usuario,
//         contrasena,
//       });

//       setUsuario(credenciales);
//       setNombre_usuario("");
//       setContrasena("");
//       setToken(credenciales.access_token)
//       setTimeout(()=>{
//         setIsLoading(false);
//       }, 2000)
//       // console.log(credenciales);
//     } catch (error) {
//       setErrorMessage(error.response.data.error);
//       console.error(error.response.data.error);
//       setTimeout(() => {
//         setErrorMessage("")
//       }, 2000);

//     }
//   };



//   useEffect(() => {
//     console.log(usuario)
//     if (usuario != null) {
//       // if (!token) {
//       setCurrentUser(usuario);
//       const now = new Date();
//       const expirationDate = new Date(now.getTime() + (usuario.expires_in * 1000));
//       // Ajusta la fecha a la zona horaria que desees

//       const expirationDateString = expirationDate.toUTCString();

//       document.cookie = `token=${token}; expires=${expirationDateString};`;
//       navigate('/home', { replace: true });
//       // }
//       // console.log("Usuario actualizado:", token);

//     }
//     // Puedes realizar acciones adicionales después de que el usuario se actualice
//   }, [usuario, navigate, setCurrentUser]);


//   return (
//     <div className="w-screen h-screen top-0 left-0 right-0 bottom-0 fixed">
//       <div
//         onClick={toggleLogin}
//         className="w-screen h-screen top-0 left-0 right-0 bottom-0 fixed bg-[#313131cc]"></div>
//       <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2">
//         <div className="w-full max-w-sm p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
//           <form className="space-y-6" onSubmit={handleLogin}>
//             <h5 className="text-xl font-medium text-gray-900 dark:text-white">
//               Iniciar Sesion
//             </h5>
//             <div>
//               <label
//                 htmlFor="usuario"
//                 className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
//                 Tu Usuario
//               </label>
//               <input
//                 type="usuario"
//                 name="usuario"
//                 id="usuario"
//                 className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
//                 placeholder="pompita123"
//                 required
//                 onChange={handleUsuarioChange}
//               />
//             </div>
//             <div>
//               <label
//                 htmlFor="password"
//                 className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
//                 Tu Contraseña
//               </label>
//               <input
//                 type="password"
//                 name="password"
//                 id="password"
//                 placeholder="••••••••"
//                 className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white"
//                 required
//                 onChange={handlePasswordChange}
//               />
//             </div>
//             <div>
//               {errorMessage && <p className="text-white">{errorMessage}</p>}
//             </div>
//             <button
//               type="submit"
//               className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
//               Iniciar Sesion
//             </button>
//             {/* <div className="text-sm font-medium text-gray-500 dark:text-gray-300">
//               Not registered?{" "}
//               <a
//                 href="#"
//                 className="text-blue-700 hover:underline dark:text-blue-500">
//                 Create account
//               </a>
//             </div> */}
//           </form>
//         </div>
//       </div>
//     </div>
//   );
// }

export default function Login() {
  const [open, setOpen] = useState(false);
  const [nombre_usuario, setNombre_usuario] = useState("");
  const [contrasena, setContrasena] = useState("");
  const { setToken, setCurrentUser, setIsLoading, currentUser } = useAuth();
  const navigate = useNavigate();
  const handleOpen = () => setOpen((cur) => !cur);

  const handleUsuarioChange = (event) => {
    setNombre_usuario(event.target.value);
  };

  const handlePasswordChange = (event) => {
    setContrasena(event.target.value);
  };

  const handleSignIn = () => {
    setIsLoading(true);
  
    loginService
      .Login({
        nombre_usuario,
        contrasena,
      })
      .then((credenciales) => {
        setToken(credenciales.access_token);
        setCurrentUser(credenciales);
        setNombre_usuario("");
        setContrasena("");
      })
      .catch((error) => {
        console.error(error.response?.data?.error || "An unexpected error occurred");
        // Handle error as needed
      })
      .finally(() => {
        setTimeout(() => {
          setIsLoading(false);
          setOpen(false);
        }, 2000);
      });
  };
  

  useEffect(() => {
    if (currentUser != null) {
      setCurrentUser(currentUser);
      const now = new Date();
      const expirationDate = new Date(
        now.getTime() + currentUser.expires_in * 1000
      );

      const expirationDateString = expirationDate.toUTCString();

      document.cookie = `token=${currentUser.access_token}; expires=${expirationDateString};`;
      navigate("/home", { replace: true });
    }
    // Puedes realizar acciones adicionales después de que el usuario se actualice
  }, [currentUser, navigate, setCurrentUser]);

  return (
    <>
      <Button onClick={handleOpen}>Sign In</Button>
      <Dialog
        size="xs"
        open={open}
        handler={handleOpen}
        className="bg-transparent shadow-none"
      >
        <Card className="mx-auto w-full max-w-[24rem]">
          <CardBody className="flex flex-col gap-4">
            <Typography variant="h4" color="blue-gray">
              Sign In
            </Typography>
            <Typography
              className="mb-3 font-normal"
              variant="paragraph"
              color="gray"
            >
              Enter your email and password to Sign In.
            </Typography>
            <Typography className="-mb-2" variant="h6">
              Your Email
            </Typography>
            <Input
              label="Email"
              size="lg"
              value={nombre_usuario}
              onChange={handleUsuarioChange}
            />
            <Typography className="-mb-2" variant="h6">
              Your Password
            </Typography>
            <Input
              label="Password"
              size="lg"
              type="password"
              value={contrasena}
              onChange={handlePasswordChange}
            />
            <div className="-ml-2.5 -mt-3">
              <Checkbox label="Remember Me" />
            </div>
          </CardBody>
          <CardFooter className="pt-0">
            <Button variant="gradient" onClick={handleSignIn} fullWidth>
              Sign In
            </Button>
            {/* Add additional UI elements as needed */}
          </CardFooter>
        </Card>
      </Dialog>
    </>
  );
}