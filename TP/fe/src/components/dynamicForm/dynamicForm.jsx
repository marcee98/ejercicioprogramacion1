// DynamicForm.jsx
import React, { useState } from 'react';
import useForm from '../../hooks/useForm';
import {
  Card,
  Input,
  Checkbox,
  Button,
  Typography,
} from "@material-tailwind/react";
import { Select, Option } from "@material-tailwind/react";

const DynamicSelect = ({ label, onChange, options }) => {
  return (
    <div>
      <Select label={label} onChange={onChange} size="md">
        {options.map((option) => (
          <Option key={option.id} value={String(option.id)}>
            {option.text}
          </Option>
        ))}
      </Select>
    </div>
  );
};

const DynamicForm = ({ fields, onSubmit, submitButtonText = 'Enviar', titulo = "Titulo", subtitulo = "" }) => {
  const initialState = Object.fromEntries(fields.map(field => [field.name, '']));
  const { formData, errors, handleChange, handleSubmit } = useForm(initialState, onSubmit);

  const [selectedOption, setSelectedOption] = useState(null);

  const handleSelectChange = (value) => {
    setSelectedOption(value);
  };

  // Dividir los campos en dos arreglos para dos columnas
  const column1Fields = fields.slice(0, Math.ceil(fields.length / 2));
  const column2Fields = fields.slice(Math.ceil(fields.length / 2));

  return (
    <Card color="transparent" shadow={false} className="min-w-[416px]">
      <Typography variant="h4" color="blue-gray">
        {titulo}
      </Typography>
      <Typography color="gray" className="mt-1 font-normal">
        {subtitulo}
      </Typography>
      <form onSubmit={handleSubmit} className="mt-8 mb-2 max-w-screen-lg">
        <div className="grid grid-flow-col auto-cols-max min-w-[416px]">
          {/* Primera columna */}
          <div className="flex flex-col gap-2">
            {column1Fields.map((field) => (
              <React.Fragment key={field.name}>
                <Typography variant="h6" color="blue-gray" className="mb-1">
                  {field.label}
                </Typography>
                {field.type === 'select' && (
                  <DynamicSelect
                    label={field.label}
                    onChange={handleSelectChange}
                    options={field.options || []}
                  />
                )}
                {field.type !== 'select' && (
                  <Input
                    size="md"
                    placeholder={`Enter ${field.label.toLowerCase()}`}
                    className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                    labelProps={{
                      className: "before:content-none after:content-none",
                    }}
                    type={field.type || 'text'}
                    id={field.name}
                    name={field.name}
                    value={formData[field.name]}
                    onChange={handleChange}
                  />
                )}
                {errors[field.name] && <span style={{ color: 'red' }}>{errors[field.name]}</span>}
              </React.Fragment>
            ))}
          </div>

          {/* Segunda columna */}
          <div className="flex flex-col gap-2 ml-4">
            {column2Fields.map((field) => (
              <React.Fragment key={field.name}>
                <Typography variant="h6" color="blue-gray" className="mb-1">
                  {field.label}
                </Typography>
                {field.type === 'select' && (
                  <DynamicSelect
                    label={field.label}
                    onChange={handleSelectChange}
                    options={field.options || []}
                  />
                )}
                {field.type !== 'select' && (
                  <Input
                    size="md"
                    placeholder={`Enter ${field.label.toLowerCase()}`}
                    className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
                    labelProps={{
                      className: "before:content-none after:content-none",
                    }}
                    type={field.type || 'text'}
                    id={field.name}
                    name={field.name}
                    value={formData[field.name]}
                    onChange={handleChange}
                  />
                )}
                {errors[field.name] && <span style={{ color: 'red' }}>{errors[field.name]}</span>}
              </React.Fragment>
            ))}
          </div>
        </div>
        <Button type="submit" className="mt-6" >
          {submitButtonText}
        </Button>
      </form>
    </Card>
  );
};


export default DynamicForm;
