import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "./pages/homePage/homePage";
import NavBar from "./components/layout/navbar";
import Animales from "./pages/animales/animales";
import ejemplo from "./components/ejemplo";
import Footer from "./components/layout/footer";
import Ayudanos from "./pages/ayudanos/ayudanos";
import Eventos from "./pages/eventos/eventos";
import AnimalDetalle from "./pages/animalDetalle/animalDetalle";
import AuthProvider from "./context/authProvider";
import { DataContext } from "./context/dataContext";
import { ProtectedRoute } from "./context/protectedRoute";
import NavBarLogged from "./components/layout/navbarLogged";
import Rescates from "./pages/rescates";
import SolicitudPublica from "./pages/solicitudPublica";

function App() {
  return (
    <>
      {/* <Router>
        <NavBar />
        <Routes>
          <Route path="/" Component={HomePage} />
          <Route path="/animales" Component={Animales} />
          <Route path="/animales:slug" element={<AnimalDetalle />} />
          <Route path="/ayudanos" Component={Ayudanos} />
          <Route path="/eventos" Component={Eventos} />
        </Routes>
        <Footer />
      </Router> */}

      {/* <Router>
        <Routes>
          <Route path="/" element={<NavBar />}>
            <Route index Component={HomePage} />
            <Route path="/animales" Component={Animales} />
            <Route path="/animales/:slug" element={<AnimalDetalle />} />
            <Route path="/ayudanos" Component={Ayudanos} />
            <Route path="/eventos" Component={Eventos} />
          </Route>
        </Routes>
        <Footer />
      </Router> */}
      <AuthProvider>
        <Router>
          <Routes>
            {/* Rutas sin Autenticación */}
            <Route path="/" element={<NavBar />}>
              <Route index Component={HomePage} />
              <Route path="/animales" Component={Animales} />
              <Route path="/animales/:slug" element={<AnimalDetalle />} />
              <Route path="/ayudanos" Component={Ayudanos} />
              <Route path="/eventos" Component={Eventos} />
              <Route path="/solicitudes" Component={SolicitudPublica} />
            </Route>

            {/* Rutas con Autenticación */}
            <Route
              path="/home"
              element={
                <DataContext>
                  <ProtectedRoute>
                    <NavBarLogged />
                  </ProtectedRoute>
                </DataContext>
              }
            >
              <Route index Component={HomePage} />
              <Route path="/home/rescates" Component={Rescates} />
              {/* <Route path="/home/rescates" Component={SolicitudPublic} /> */}
              {/* <Route index Component={HomePage} /> */}
            </Route>
          </Routes>
        </Router>
        <Footer />
      </AuthProvider>

    </>
  );
}

export default App;

{/* <AuthProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={
            <DataContext>
            <ProtectedRoute>

                <NavBbar />


            </ProtectedRoute>
            </DataContext>} >
            <Route path="/home" element={<Home />} />
            <Route exact path="/" element={<Home />} />
            <Route path="/deudas" element={<Deudas />} />
            <Route path="/perfil" element={<Profile />} />
          </Route>
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />

        </Routes>
      </BrowserRouter>
    </AuthProvider> */}
