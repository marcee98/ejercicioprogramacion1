import { createContext } from "react";

const Context = createContext();

export default function AuthContext({ children }) {
  const user = { logged: true };
  return <Context.Provider value={{ user }}>{children}</Context.Provider>;
}
