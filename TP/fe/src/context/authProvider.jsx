import { useContext } from 'react';
import { createContext } from "react";
import { useEffect } from 'react';
import { useState } from 'react';

export const authContext = createContext();

export const useAuth = () => {
    const context = useContext(authContext);
    return context
}

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {

    const [token, setToken] = useState(null);

    const [currentUser, setCurrentUser] = useState(null);

    const [isLoading, setIsLoading] = useState(false);



    useEffect(() => {

    }, [])



    return (

        <authContext.Provider value={{ currentUser, setCurrentUser, isLoading, setIsLoading, token, setToken }}>
            
            {children}
        </authContext.Provider>
    )
}

export default AuthProvider