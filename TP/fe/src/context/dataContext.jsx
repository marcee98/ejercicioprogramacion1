
import React from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import { createContext } from 'react';
import { useContext } from 'react';


export const dataContext = createContext();

export const useGlobalData = () => {
    const dContext = useContext(dataContext);
    return dContext
}



export const DataContext = ({children}) => {

    const [datta, setDatta] = useState([]);

    // const [loadingData, setLoadingData] = useState(true);

    // // 2. referenciamos a la db
    // const gastosCollection = collection(db, "gastos");

    // // 3. funcion para mostrar todos los docs
    // const getGastos = async () => {
    //     setLoadingData(true)
    //     const data = await getDocs(gastosCollection);

    //     setDatta(

    //         data.docs.map(doc => {

    //             return {
    //                 ...doc.data()
    //             }
    //         })

    //     )
    //     setLoadingData(false)
    //     // console.log(gastoT)
    //     // console.log(gastos)
    // }

    // useEffect(() => {
    //     getGastos()

    //     // unsuscribe();
    // }, [])






    return (
        <dataContext.Provider value={{ datta }}>

            {children}
        </dataContext.Provider>
    )
}