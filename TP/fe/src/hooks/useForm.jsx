// useForm.jsx
import { useState } from 'react';

const useForm = (initialState, submitCallback) => {
  const [formData, setFormData] = useState(initialState);
  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
    setErrors({ ...errors, [name]: null });
  };

  const validateForm = () => {
    let formIsValid = true;
    const newErrors = {};

    // Implement custom validation logic based on form fields
    // Example: 
    // if (!formData.email || !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(formData.email)) {
    //   newErrors.email = 'Ingrese un correo electrónico válido.';
    //   formIsValid = false;
    // }

    setErrors(newErrors);
    return formIsValid;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      submitCallback(formData);
    }
  };

  return { formData, errors, handleChange, handleSubmit };
};

export default useForm;
