<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Productos</title>
    <style>
        table {
            border-collapse: collapse;
            margin: 20px auto;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: center;
        }
        th {
            background-color: gray;
        }
        .header-row th {
            background-color: yellow;
        }
        tr:nth-child(even) {
            background-color: #90EE90; 
        }
    </style>
</head>
<body>
    <?php
    // Datos de los productos
    $productos = array(
        array("Coca Cola", 100, 4500),
        array("Pepsi", 30, 4800),
        array("Sprite", 20, 4500),
        array("Guaraná", 200, 4500),
        array("SevenUp", 24, 4800),
        array("Mirinda Naranja", 56, 4800),
        array("Mirinda Guaraná", 89, 4800),
        array("Fanta Naranja", 10, 4500),
        array("Fanta Piña", 2, 4500),
    );


    // Genera tabla usando datos
    echo "<table>";
    echo "<tr class='header-row'>";
    echo "<th colspan='3'>Productos</th>";
    echo "<tr>";
    echo "<th>Nombre</th>";
    echo "<th>Cantidad</th>";
    echo "<th>Precio (Gs)</th>";
    echo "</tr>";

    foreach ($productos as $producto) {
        echo "<tr>";
        echo "<td>" . $producto[0] . "</td>";
        echo "<td>" . $producto[1] . "</td>";
        echo "<td>" . number_format($producto[2]) . "</td>";
        echo "</tr>";
    }

    echo "</table>";
    ?>
</body>
</html>
