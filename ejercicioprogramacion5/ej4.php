<?php
// Datos de conexión a la base de datos PostgreSQL
$dbhost = "localhost";
$dbname = "ejercicio1";
$dbuser = "postgres";
$dbpass = "123";
$dbport = "5433";


try {
    // Conexión a la base de datos utilizando PDO
    // $pdo = new PDO("pgsql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $pdo = new PDO("pgsql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);


    // Consulta SQL para obtener los datos requeridos
    $query = "SELECT p.nombre AS nombre_producto, p.precio, m.nombre AS nombre_marca, e.nombre AS nombre_empresa, c.nombre AS nombre_categoria
          FROM producto p
          INNER JOIN marca m ON p.id_marca = m.id_marca
          INNER JOIN empresa e ON m.id_empresa = e.id_empresa
          INNER JOIN categoria c ON p.id_categoria = c.id_categoria";
    // Preparar y ejecutar la consulta
    $stmt = $pdo->query($query);

    // Establecer el modo de recuperación de datos a objetos
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die("Error de conexión: " . $e->getMessage());
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Productos</title>
</head>
<body>
    <h1>Productos</h1>
    <table>
        <tr>
            <th>Producto</th>
            <th>Precio</th>
            <th>Marca</th>
            <th>Empresa</th>
            <th>Categoría</th>
        </tr>
        <?php
        while ($row = $stmt->fetch()) {
            echo "<tr>";
            echo "<td>" . $row['nombre_producto'] . "</td>";
            echo "<td>" . $row['precio'] . "</td>";
            echo "<td>" . $row['nombre_marca'] . "</td>";
            echo "<td>" . $row['nombre_empresa'] . "</td>";
            echo "<td>" . $row['nombre_categoria'] . "</td>";
            echo "</tr>";
        }
        ?>
    </table>
</body>
</html>